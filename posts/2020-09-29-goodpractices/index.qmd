---
title: "goodpractice - Conseils de développement de vos packages"
author: "I.Sanchez (MISTEA)"
date: 2020-09-29T10:09:13-06:00
categories: [packages, code, R]
toc: true
number-sections: true
tags: ["developpement R", "devtools", "goodpractice", "ROpenSci", "CRAN", "bioconductor"]
---

<div style="background-color:rgba(0, 255, 0,0.1); text-align:left; vertical-align: center; padding:10px 0;">
L'objectif de ce document est de présenter le package **goodpractice**, package permettant de contrôler la structure et le style de votre package suivant de "bonnes pratiques" de codage.

https://cran.r-project.org/web/packages/goodpractice/index.html
</div>

# Package goodpractice

![](logo_goodpractice.png){width="100"}

Goodpractice est un package disponible sur le CRAN et github, développé par Gabor Csardi (le créateur de **igraph**, entre autres...) et Hannah Frick. Il permet de vérifier la structure, la syntaxe, la complexité du codage d'un package en cours de développement.

Il est bien évident qu'il n'est pas obligatoire de tout prendre pour argent comptant mais j'ai trouvé que ça donnait de bons conseils globalement. Super rapide, on peut voir très facilement où améliorer notre package.

Il peut être utilisé également comme critère supplémentaire dans l'évaluation d'un package que l'on souhaite tester.

Je l'ai maintenant intégré dans ma procédure de développement de package R que je souhaite déposer sur le CRAN, Bioconductor ou RopenSci.

découverte de **goodpractice** dans le guide de développement de package de **ROpenSci**:

* https://devguide.ropensci.org/building.html#cranchecks

# Installation

```{r, eval=FALSE}
install.packages("goodpractice")
```

# Usage

```{r,eval=FALSE}
library(goodpractice)
gp("<chemin_vers_mon_package/mon-package>")
```


# Références

1. https://cran.r-project.org/web/packages/goodpractice/index.html
2. https://github.com/MangoTheCat/goodpractice
3. http://mangothecat.github.io/goodpractice/
4. https://devguide.ropensci.org/preface.html
5. https://github.com/MangoTheCat

# Session Informations

```{r, echo=FALSE,error=TRUE}
  sessionInfo()
```

