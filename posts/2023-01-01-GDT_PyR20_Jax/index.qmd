---
title: "GdT-Pyr 20 - Jax"
author: "A. Cotil"
date: 2023-01-01T21:13:14-05:00
tags: ["Jax","tensoriel","differentiation","high-performance"]
categories: [code, Python, calculs]
toc: true
number-sections: true
---


<div style="background-color:rgba(0, 255, 0,0.1); text-align:left; vertical-align: center; padding:10px 0;">
Séance 20 du groupe de travail PyR présentant le calcul tensoriel avec **Jax** dans Python:

Le notebook est visible dans le repo suivant:

https://forgemia.inra.fr/gdtpyr/gdt_pyr/-/tree/main/GDT_PYR_20_Jax?ref_type=heads

https://jax.readthedocs.io/en/latest/notebooks/quickstart.html
</div>

**Jax** est un outil de calcul tensoriel permettant d'utiliser le compilateur XLA sur Python. 

![](logo_jax.jpg){width="100"}

Ses principaux avantages sont :

1. Acceleration du calcul tensoriel.
2. Gestion automatique de l'utilisation du CPU, GPU (et TPU).
3. Opérations complexes automatisées telles que l'autodérivation (utile pour faire de l'optimisation).
4. Construit exactement comme Numpy pour faciliter sa prise en main.

https://jax.readthedocs.io/en/latest/notebooks/quickstart.html
