---
title: "GdT-Pyr 10 - Création de TDs, examens, QCMs avec R"
author: "B. Fontez, M. Baragatti & I. Sanchez"
date: 2019-11-04T21:13:14-05:00
tags: ["learnr", "exams","enseignement"]
categories: [Enseignement, code, R]
toc: true
number-sections: true
---


<div style="background-color:rgba(0, 255, 0,0.1); text-align:left; vertical-align: center; padding:10px 0;">
L'objectif de ce document est de décrire les différents packages R permettant de créer des TDs, des examens, des QCMs etc...

* la librairie __learnr__: https://rstudio.github.io/learnr/index.html
* la librairie __exams__: http://www.r-exams.org/ - http://www.r-exams.org/resources/

Code complet: https://forgemia.inra.fr/gdtpyr/gdt_pyr/-/tree/main/GDT_PyR10_examensR
</div>

# package learnr

![](logo_learnr.png){width="100"}

```{r, echo=TRUE, eval=FALSE}
install.packages("learnr")
```

__learnr__ est un package R, développé par Rstudio, nécessitant l'installation de shiny et rmarkdown. En effet, __learnr__ repose sur ces 2 packages, les examens sont écrits dans un document __Rmarkdown__ et executés comme une appli __shiny__:

* soit en le déposant sur un site web avec serveur R et Rshiny (__shinyapps.io__ par exemple)
* soit par execution en local: 

```{r, echo=TRUE, eval=FALSE}
  rmarkdown::run(paste0(getwd(),"/GDT_PyR10_examensR/Ex1_tutoR.Rmd"))
```

En executant la ligne ci-dessus, le tutoriel s'ouvre dans le viewer de Rstudio et peut être ouvert dans votre browser habituel.

Sur le site de __learnr__ quelques exemples sont disponibles: https://rstudio.github.io/learnr/examples.html, avec leurs codes sous github.

Le lien suivant [exercises](https://rstudio.github.io/learnr/exercises.html) décrit toutes les options des _r code chunks_ possibles pour les exercices proposées aux étudiants: 

* exercise.cap: Caption for exercise chunk (defaults to “Code”)
* exercise.eval: Whether to pre-evaluate the exercise so the reader can see some default output (defaults to FALSE).
* exercise.lines: Lines of code for exercise editor (default to size of code chunk).
* exercise.timelimit: Number of seconds to limit execution time to (defaults to 30).
* exercise.checker: Function used to check exercise answers.
* exercise.completion: Whether to enable code completion in the exercise editor.
* exercise.diagnostics: Whether to enable code diagnostics in the exercise editor.
* exercise.startover: Whether to include a “Start Over” button for the exercise. 

A noter qu'un tutoriel n'est pas un simple fichier .Rmd mais un répertoire contenant le fichier .Rmd avec les fichiers adjacents (images, données etc...). A un tutoriel correspond un répertoire si on veut bien organiser ces examens!

Malheureusement, les tuto créés ne peuvent utiliser que le langage R, j'ai testé avec le package __reticulate__ qui fonctionne très bien avec un .Rmd traditionel mais pas avec un .Rmd de __learnr__... Quel dommage!

Le présent répertoire contient un exemple produit avec __learnr__: 

* Ex1_tutoR.Rmd le rmarkdown contenant tous le code. Vous pouvez voir les mots-clés spécifiques utilisés dans les chunks.
* Ex1_tuto.html: le TD produit avec le rmarkdown précédent.

Pour rappel, le html ne peut s'ouvrir tel quel (du moins, il nemontrera pas la structure learnr), il faut executer le .Rmd avec la commande suivante:

```{r, echo=TRUE, eval=FALSE}
  rmarkdown::run(paste0(getwd(),"/GDT_PyR10_examensR/Ex1_tutoR.Rmd"))
```

# package exams

```{r, echo=TRUE, eval=FALSE}
install.packages("exams", dependencies = TRUE)
```

__exams__ est un package R disponible sur le [CRAN](https://cran.r-project.org/package=exams). Il permet de construire des examens de manière automatique au format markdown ou LaTeX et incluant des chunks de code R dynamique. Comme pour __learnr__ les exercices peuvent être à choix multiples ou simples, des problèmes arithmétiques, du code...

Un examen __exams__ a plusieurs sorties possibles:

* fichiers autonomes : PDF, HTML, Docx, ODT, ...
* fichiers dynamiques: Moodle XML, QTI 1.2, QTI 2.1, Blackboard, Canvas, OpenOLAT, ARSnova, and TCExam
* possibilité de scanner les feuilles d'examens imprimés et de les évaluer automatiquement: NOPS

Une autre option intéressante, pour réduire le risque de triche, __exams__ propose un mécanisme de variations aléatoires des exercices:

* mélange de l'ordre des questions
* mélange des réponses possibles pour les QCMs
* mélange des données des exercices

Vous pouvez voir quelques exemples, fournis par Bénédicte, dans le sous-répertoire __fichiers Exam__. 

## Exemple d'un examen

Hiérarchie des fichiers:

* Test2019Q1.Rmd, Test2019Q2.Rmd, Test2019T.Rmd, Test2019Tension.Rmd: contiennent dans chaque fichier une question ou ensemble de questions
* Test-exam-pdf.R: le script regroupant les questions ainsi que la structure de l'examen (questions ouvertes, fermées, à choix multiples, simple etc...)
* pdf-exam1.pdf et pdf-solution1.pdf: le résultat de l'exécution du script précédent et donc ce que l'on donne aux étudiants

## Exemple d'un examen sur l'ACP

Hiérarchie des fichiers:

* ACP2019.Rmd: contient l'ensemble de questions
* ACP-exam-pdf.R: le script regroupant les questions ainsi que la structure de l'examen (questions ouvertes, fermées, à choix multiples, simple etc...)
* ACP-exam1.pdf et ACP-solution1.pdf: le résultat de l'exécution du script précédent et donc ce que l'on donne aux étudiants

# Divers langages

Il est également possible de créer des quizzes très rapidement pour n'importe quel langage (ou thème). Les étudiants peuvent soit tester leurs connaissances chez eux pour s'entrainer soit faire un quizz en classe pour démarrer/finir une séance:

* quizzlet: https://quizlet.com/fr-fr
* kahoot: https://kahoot.com/

# Session Informations

```{r, echo=FALSE,error=TRUE}
  sessionInfo()
```
