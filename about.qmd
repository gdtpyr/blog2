---
title: "About"
image: PyR_logo.PNG
about:
  template: trestles
---

This is a blog website for the GdT_PyR group of the MISTEA lab using the [**quarto**](https://quarto.org/) package.

The GdT-PyR provides some tips for **R** and **Python** languages, feel free to browse the blog. A gitlab repository is linked to this blog, describing in full details the posts in the 2 languages:

https://forgemia.inra.fr/gdtpyr/gdt_pyr

© 2020-2024 - GPL3 License - INRAE MISTEA

Creation: [Isabelle Sanchez](https://mistea.pages.mia.inra.fr/isabellesanchez/) (INRAE MISTEA) with the useful help of [Arnaud Charleroy](https://forgemia.inra.fr/arnaud.charleroy) (INRAE MISTEA) and the GdT-PyR group.

![](INRAE_logo.png){width="100"}

![INRAE - Mathnum](logo_mathnum.jpg){width="200"}
